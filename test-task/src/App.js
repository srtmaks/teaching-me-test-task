import "./App.css";
import React, { useEffect, useState } from "react";
import check from "./icons/fact_check_FILL0_wght400_GRAD0_opsz48.png";
import cancel from "./icons/cancel_FILL0_wght400_GRAD0_opsz48.png";

function App() {
  let categories = [];
  let teachers = [];
  let allTeachers = [];
  let prices = [];

  // async function postPrice() {
  //   try {
  //     const response = await fetch(
  //       "http://test.teaching-me.org/categories/v1/average-price",
  //       {
  //         method: "POST",
  //         headers: {
  //           "Content-Type": "application/json",
  //         },
  //         body: '{"categoryName" : "English", "averagePrice" : 123.5}',
  //       }
  //     );
  //     const data = await response.json();
  //     console.log("response data?", data);
  //   } catch (err) {
  //     console.log("Error happened here!");
  //     console.log(err);
  //   }
  // }

  async function calculatePrice() {
    try {
      const response = await fetch(
        "http://test.teaching-me.org/categories/v1/categories",
        {
          headers: {
            "Accept-Language": "en",
          },
        }
      );
      const data = await response.json();
      for (let i = 0; i < data.length; i++) {
        categories.push({
          name: data[i].name,
          code: data[i].code,
        });
        for (let j = 0; j < data[i].childrenCategories.length; j++) {
          categories.push({
            name: data[i].childrenCategories[j].name,
            code: data[i].childrenCategories[j].code,
            price: "",
          });
        }
      }
      categories.sort((a, b) => {
        return a.code - b.code;
      });
    } catch (err) {}

    const makeInit = (num) => ({
      method: "POST",
      headers: {
        "Accept-Language": "en",
        "Content-Type": "application/json",
      },
      body: `{"categories": [${num}], "page": 0, "pageSize": 10}`,
    });

    for (let num = 1; num <= categories.length; num++) {
      let response = await fetch(
        "http://test.teaching-me.org/categories/v1/search",
        makeInit(num)
      );
      let data = await response.json();
      teachers.push(data.totalResults);
    }

    const makeInit2 = (num) => ({
      method: "POST",
      headers: {
        "Accept-Language": "en",
        "Content-Type": "application/json",
      },
      body: `{"categories": [${num}], "page": 0, "pageSize": ${
        teachers[num - 1]
      }}`,
    });

    for (let num = 1; num <= categories.length; num++) {
      let response = await fetch(
        "http://test.teaching-me.org/categories/v1/search",
        makeInit2(num)
      );
      let data = await response.json();
      allTeachers.push(data);
    }

    for (let i = 0; i < allTeachers.length; i++) {
      let price = 0;
      for (let j = 0; j < allTeachers[i].teachers.length; j++) {
        price = price + allTeachers[i].teachers[j].pricePerHour;
      }
      prices.push(price);
    }
    for (let n = 0; n < allTeachers.length; n++) {
      prices[n] = prices[n] / teachers[n];
      prices[n] = prices[n].toFixed(1);
      categories[n].price = prices[n];
    }
    console.log(categories);
  }

  return (
    <div className="App">
      <button
        className="btn-calculate"
        onClick={() => {
          calculatePrice();
        }}
      >
        Calculate average Price
      </button>
      {/* <button
        onClick={() => {
          postPrice();
        }}
      >
        Post
      </button> */}

      <div className="markup">
        <div className="card">
          <div className="message">
            <div className="message-img">
              <img className="img" src={check} alt="check" />
            </div>
            <div className="message-text">
              <p className="title">Request for the lesson</p>
              <p className="text">
                Daniel Hamilton wants to start a lesson, please confirm or deny
                the request
              </p>
              <p className="date">18 Dec, 14:50px, 2022</p>
            </div>
            <div className="message-close">
              <img className="close-icon" src={cancel} alt="cancel" />
            </div>
          </div>
          <div className="actions">
            <button className="details">View details</button>
            <button className="submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
